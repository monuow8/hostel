/*
SQLyog Trial v11.31 (64 bit)
MySQL - 5.6.15-log : Database - hostelgraph
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hostelgraph` /*!40100 DEFAULT CHARACTER SET tis620 */;

USE `hostelgraph`;

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `NIN` varchar(20) NOT NULL,
  `Nationality` varchar(25) NOT NULL,
  `IsMember` bit(1) NOT NULL,
  PRIMARY KEY (`NIN`,`Nationality`)
) ENGINE=InnoDB DEFAULT CHARSET=tis620;

/*Data for the table `customer` */

insert  into `customer`(`NIN`,`Nationality`,`IsMember`) values ('1102000665245','Thai',''),('111265486','American','\0'),('445548946134','Italian','\0'),('855965486','American',''),('AA100054','Thai','\0'),('AA589654','Thai',''),('AA589999','Thai',''),('IT55653AA','Italian',''),('IT55653BB','Italian',''),('IT55653CC','Italian','');

/*Table structure for table `hostelusage` */

DROP TABLE IF EXISTS `hostelusage`;

CREATE TABLE `hostelusage` (
  `NIN` varchar(20) NOT NULL,
  `RoomID` int(11) NOT NULL,
  `CheckIn` datetime NOT NULL,
  `CheckOut` datetime NOT NULL,
  `UID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=tis620 COMMENT='Summary of Hotel usage';

/*Data for the table `hostelusage` */

insert  into `hostelusage`(`NIN`,`RoomID`,`CheckIn`,`CheckOut`,`UID`) values ('1102000665245',1,'2014-01-05 00:00:00','2014-01-06 00:00:00',1),('111265486',1,'2014-01-06 00:00:00','2014-01-07 00:00:00',2),('445548946134',2,'2014-01-15 00:00:00','2014-01-25 00:00:00',3),('855965486',3,'2014-01-01 00:00:00','2014-01-03 00:00:00',4),('445548946134',4,'2014-01-15 00:00:00','2014-01-25 00:00:00',5),('AA100054',4,'2014-01-10 00:00:00','2014-01-13 00:00:00',6),('AA589654',1,'2014-01-02 00:00:00','2014-01-04 00:00:00',7),('AA589999',1,'2014-01-04 00:00:00','2014-01-05 00:00:00',8),('IT55653AA',2,'2014-01-29 00:00:00','2014-02-02 00:00:00',9),('IT55653BB',3,'2014-01-29 00:00:00','2014-02-05 00:00:00',10),('IT55653CC',1,'2014-01-17 00:00:00','2014-01-19 00:00:00',11),('445548946134',3,'2014-01-05 00:00:00','2014-01-10 00:00:00',12),('855965486',3,'2014-01-12 00:00:00','2014-01-15 00:00:00',13),('445548946134',2,'2014-01-08 00:00:00','2014-01-09 00:00:00',14),('AA100054',2,'2014-01-09 00:00:00','2014-01-12 00:00:00',15),('AA589654',4,'2014-01-13 00:00:00','2014-01-14 00:00:00',16),('AA589999',4,'2014-01-14 00:00:00','2014-01-15 00:00:00',17),('IT55653AA',4,'2014-01-30 00:00:00','2014-02-02 00:00:00',18),('IT55653BB',1,'2014-01-13 00:00:00','2014-02-15 00:00:00',19),('IT55653CC',1,'2014-01-16 00:00:00','2014-01-17 00:00:00',20),('IT55653CC',1,'2013-01-01 00:00:00','2013-01-01 00:00:00',21),('AA589999',2,'2013-01-02 00:00:00','2013-01-03 00:00:00',22),('1102000665245',1,'2013-01-05 00:00:00','2013-01-06 00:00:00',100),('111265486',1,'2013-01-06 00:00:00','2013-01-07 00:00:00',101),('445548946134',2,'2013-01-15 00:00:00','2013-01-25 00:00:00',102),('855965486',3,'2013-01-01 00:00:00','2013-01-03 00:00:00',103),('445548946134',4,'2013-01-15 00:00:00','2013-01-25 00:00:00',104),('AA100054',4,'2013-01-10 00:00:00','2013-01-13 00:00:00',105),('AA589654',1,'2013-01-02 00:00:00','2013-01-04 00:00:00',106),('AA589999',1,'2013-01-04 00:00:00','2013-01-05 00:00:00',107),('IT55653AA',2,'2013-01-29 00:00:00','2013-02-02 00:00:00',108),('IT55653BB',3,'2013-01-29 00:00:00','2013-02-05 00:00:00',109),('IT55653CC',1,'2013-01-17 00:00:00','2013-01-19 00:00:00',110),('445548946134',3,'2013-01-05 00:00:00','2013-01-10 00:00:00',111),('855965486',3,'2013-01-12 00:00:00','2013-01-15 00:00:00',112),('445548946134',2,'2014-05-08 00:00:00','2014-05-09 00:00:00',113),('AA100054',2,'2014-02-09 00:00:00','2014-02-12 00:00:00',114),('AA589654',4,'2014-02-13 00:00:00','2014-02-14 00:00:00',115),('AA589999',4,'2014-02-14 00:00:00','2014-02-15 00:00:00',116),('IT55653AA',4,'2014-03-30 00:00:00','2014-04-02 00:00:00',117),('IT55653BB',1,'2014-03-13 00:00:00','2014-04-15 00:00:00',118),('IT55653CC',1,'2013-01-16 00:00:00','2013-08-17 00:00:00',119),('IT55653CC',1,'2014-08-01 00:00:00','2014-08-02 00:00:00',120),('AA589999',2,'2014-08-02 00:00:00','2014-08-03 00:00:00',121),('AA589654',1,'2014-01-01 00:00:00','2014-01-02 00:00:00',122),('IT55653AA',1,'2014-01-31 00:00:00','2014-02-01 00:00:00',123),('1102000665245',2,'2013-12-30 00:00:00','2014-01-05 00:00:00',124),('AA589999',6,'2013-12-06 00:00:00','2014-04-17 00:00:00',125),('AA100054',6,'2014-01-16 00:00:00','2014-01-17 00:00:00',126),('AA100054',3,'2014-01-16 00:00:00','2014-01-18 00:00:00',127);

/*Table structure for table `room` */

DROP TABLE IF EXISTS `room`;

CREATE TABLE `room` (
  `RoomID` int(11) NOT NULL AUTO_INCREMENT,
  `RoomType` varchar(30) NOT NULL,
  `PriceMember` int(11) NOT NULL,
  `PriceNonMember` int(11) NOT NULL,
  PRIMARY KEY (`RoomID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=tis620 COMMENT='room detail';

/*Data for the table `room` */

insert  into `room`(`RoomID`,`RoomType`,`PriceMember`,`PriceNonMember`) values (1,'1P',800,1200),(2,'2P',1200,1500),(3,'3P',1500,1800),(4,'VIP',500,600),(5,'Dorm',400,500),(6,'Suite',400,2000);

/* Procedure structure for procedure `GetIncomeMonthly` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetIncomeMonthly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `GetIncomeMonthly`(IN fromDate Date,IN toDate Date)
BEGIN
	select h.checkin ,h.checkout 
		,case when c.ismember then r.PriceMember 
			else r.PriceNonMember end Price
	from hostelusage h
	inner join customer c
		on h.nin = c.nin
	inner join room r
		on h.roomid = r.roomid
	where (h.checkin between fromDate and toDate
	OR h.checkout between fromDate and toDate ) 
	OR (h.checkin < fromDate AND h.checkout > toDate)
	order by h.checkIn;
END */$$
DELIMITER ;

/* Procedure structure for procedure `GetLodgerMonthly` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetLodgerMonthly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `GetLodgerMonthly`(IN fromDate Date,IN toDate Date)
BEGIN
SELECT CheckIn,COUNT(DISTINCT NIN) Qty
	FROM hostelusage 
	where CheckIn between fromDate and toDate
GROUP BY checkin;
		
END */$$
DELIMITER ;

/* Procedure structure for procedure `GetNationalityMonthly` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetNationalityMonthly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `GetNationalityMonthly`(IN fromDate Date,IN toDate Date)
BEGIN
	
	SELECT c.Nationality 
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 0 THEN 1 ELSE 0 END) Day1
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 1 THEN 1 ELSE 0 END) Day2
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 2 THEN 1 ELSE 0 END) Day3
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 3 THEN 1 ELSE 0 END) Day4
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 4 THEN 1 ELSE 0 END) Day5
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 5 THEN 1 ELSE 0 END) Day6
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 6 THEN 1 ELSE 0 END) Day7
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 7 THEN 1 ELSE 0 END) Day8
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 8 THEN 1 ELSE 0 END) Day9
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 9 THEN 1 ELSE 0 END) Day10
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 10 THEN 1 ELSE 0 END) Day11
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 11 THEN 1 ELSE 0 END) Day12
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 12 THEN 1 ELSE 0 END) Day13
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 13 THEN 1 ELSE 0 END) Day14
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 14 THEN 1 ELSE 0 END) Day15
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 15 THEN 1 ELSE 0 END) Day16
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 16 THEN 1 ELSE 0 END) Day17
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 17 THEN 1 ELSE 0 END) Day18
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 18 THEN 1 ELSE 0 END) Day19
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 19 THEN 1 ELSE 0 END) Day20
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 20 THEN 1 ELSE 0 END) Day21
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 21 THEN 1 ELSE 0 END) Day22
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 22 THEN 1 ELSE 0 END) Day23
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 23 THEN 1 ELSE 0 END) Day24
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 24 THEN 1 ELSE 0 END) Day25
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 25 THEN 1 ELSE 0 END) Day26
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 26 THEN 1 ELSE 0 END) Day27
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 27 THEN 1 ELSE 0 END) Day28
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 28 THEN 1 ELSE 0 END) Day29
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 29 THEN 1 ELSE 0 END) Day30
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 30 THEN 1 ELSE 0 END) Day31
	FROM hostelusage h 
	INNER JOIN customer c 
		ON h.NIN = c.NIN
	WHERE h.Checkin BETWEEN fromDate AND toDate
	GROUP BY c.Nationality;
		
END */$$
DELIMITER ;

/* Procedure structure for procedure `GetRoomUsageMonthly` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetRoomUsageMonthly` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `GetRoomUsageMonthly`(IN fromDate Date,IN toDate Date)
BEGIN
	SELECT r.RoomType 
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 0 THEN 1 ELSE 0 END) Day1
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 1 THEN 1 ELSE 0 END) Day2
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 2 THEN 1 ELSE 0 END) Day3
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 3 THEN 1 ELSE 0 END) Day4
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 4 THEN 1 ELSE 0 END) Day5
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 5 THEN 1 ELSE 0 END) Day6
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 6 THEN 1 ELSE 0 END) Day7
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 7 THEN 1 ELSE 0 END) Day8
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 8 THEN 1 ELSE 0 END) Day9
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 9 THEN 1 ELSE 0 END) Day10
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 10 THEN 1 ELSE 0 END) Day11
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 11 THEN 1 ELSE 0 END) Day12
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 12 THEN 1 ELSE 0 END) Day13
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 13 THEN 1 ELSE 0 END) Day14
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 14 THEN 1 ELSE 0 END) Day15
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 15 THEN 1 ELSE 0 END) Day16
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 16 THEN 1 ELSE 0 END) Day17
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 17 THEN 1 ELSE 0 END) Day18
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 18 THEN 1 ELSE 0 END) Day19
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 19 THEN 1 ELSE 0 END) Day20
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 20 THEN 1 ELSE 0 END) Day21
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 21 THEN 1 ELSE 0 END) Day22
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 22 THEN 1 ELSE 0 END) Day23
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 23 THEN 1 ELSE 0 END) Day24
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 24 THEN 1 ELSE 0 END) Day25
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 25 THEN 1 ELSE 0 END) Day26
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 26 THEN 1 ELSE 0 END) Day27
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 27 THEN 1 ELSE 0 END) Day28
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 28 THEN 1 ELSE 0 END) Day29
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 29 THEN 1 ELSE 0 END) Day30
	,SUM(CASE WHEN DATEDIFF(h.CheckIn,fromDate) = 30 THEN 1 ELSE 0 END) Day31
	FROM room r
	LEFT JOIN hostelusage h 
		ON h.RoomID = r.RoomId
	WHERE (h.Checkin BETWEEN fromDate AND toDate)
		OR r.RoomID is not null
	GROUP BY r.RoomType
	ORDER BY r.RoomID;
		
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
