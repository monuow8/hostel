﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphModule.Model
{
    public class FormSelection
    {
        public int Month { get; set; }
        public int MonthToCompare { get; set; }
        public int Year { get; set; }
        public int YearToCompare { get; set; }
        public bool IsMonthly { get; set; }
    }
}