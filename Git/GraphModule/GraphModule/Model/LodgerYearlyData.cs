﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphModule.Model
{
    public class LodgerYearlyData
    {
        public int year { get; set; }
        public List<LodgerMonthSummary> MonthList { get; set; }
    }

    public class LodgerMonthSummary
    {
        public int Month { get; set; }
        public int Quantity { get; set; }
    }
}