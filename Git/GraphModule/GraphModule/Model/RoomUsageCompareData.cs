﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphModule.Model
{
    public class RoomUsageCompareData
    {
        public string RoomType { get; set; }
        public int[] Values { get; set; }
    }
}