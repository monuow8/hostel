﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphModule.Model
{
    public class NationalityYearlyData
    {
        public string Nationality { get; set; }
        public List<NationalityMonthSummary> NationalityMonthSummaryList { get; set; }
    }

    public class NationalityMonthSummary
    {
        public int Month { get; set; }
        public int Quantity { get; set; }
        public string Nationality { get; set; }
    }
}