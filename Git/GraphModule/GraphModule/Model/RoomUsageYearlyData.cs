﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphModule.Model
{
    public class RoomUsageYearlyData
    {
        public string RoomType { get; set; }
        public List<RoomUsageMonthSummary> RoomUsageSummaryList { get; set; }
    }

    public class RoomUsageMonthSummary
    {
        public int Month { get; set; }
        public int Quantity { get; set; }
        public string RoomType { get; set; }
    }
}