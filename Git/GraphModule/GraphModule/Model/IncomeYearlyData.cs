﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphModule.Model
{
    public class IncomeYearlyData
    {
        public int year { get; set; }
        public List<IncomeMonthSummary> MonthList { get; set; }
    }

    public class IncomeMonthSummary
    {
        public int Month { get; set; }
        public int Income { get; set; }
    }
}