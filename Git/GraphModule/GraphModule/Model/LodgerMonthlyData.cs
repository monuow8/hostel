﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphModule.Model
{
    public class LodgerMonthlyData
    {
        public DateTime CheckIn { get; set; }
        public int Qty { get; set; }
    }
}