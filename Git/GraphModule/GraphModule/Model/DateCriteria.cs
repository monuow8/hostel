﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace GraphModule.Model
{
    public class DateCriteria
    {
        public static List<Month> MonthList;
        public static List<Year> YearList;

        public DateCriteria()
        {
            MonthList = new Month().GetMonth();
            YearList = new Year().GetYear();
        }
    }

    public class Month
    {
        public string Text { get; set; }
        public int Value { get; set; }

        public List<Month> GetMonth()
        {
            List<Month> list = new List<Month>();
            Month m = new Month() { Text = "--- Month ---", Value = 0 };
            list.Add(m);
            m = new Month() { Text = " January ", Value = 1 };
            list.Add(m);
            m = new Month() { Text = " Febuary ", Value = 2 };
            list.Add(m);
            m = new Month() { Text = " March ", Value = 3 };
            list.Add(m);
            m = new Month() { Text = " April ", Value = 4 };
            list.Add(m);
            m = new Month() { Text = " May", Value = 5 };
            list.Add(m);
            m = new Month() { Text = " June", Value = 6 };
            list.Add(m);
            m = new Month() { Text = " July ", Value = 7 };
            list.Add(m);
            m = new Month() { Text = " August ", Value = 8 };
            list.Add(m);
            m = new Month() { Text = " September ", Value = 9 };
            list.Add(m);
            m = new Month() { Text = " October ", Value = 10 };
            list.Add(m);
            m = new Month() { Text = " November ", Value = 11 };
            list.Add(m);
            m = new Month() { Text = " December ", Value = 12 };
            list.Add(m);

            return list;
        }
    }

    public class Year
    {
        public string Text { get; set; }

        public List<Year> GetYear()
        {
            List<Year> list = new List<Year>();
            string config = WebConfigurationManager.AppSettings["AvailableYear"];
            string[] years = config.Split(',');
            Year y;
            y = new Year() { Text = "--- Year ---" };
            list.Add(y);

            foreach (string item in years)
            {
                y = new Year() { Text = item };
                list.Add(y);
            }
            return list;
        }
    }
}