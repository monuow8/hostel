﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GraphModule.Model
{
    public class NationalityCompareData
    {
        public string Nationality {get;set;}
        public int[] Values { get; set; }
    }
}