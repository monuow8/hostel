﻿using GraphModule.Graph;
using GraphModule.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GraphModule
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // initialize Month , Year list
            DateCriteria dateCriteria = new DateCriteria();

            //Check latest data
            GraphController controller = new GraphController();
            DateTime latestDate = controller.GetLatestDataDate();
            if (latestDate < DateTime.Today)
            {
                controller.UpdateHostelUsage(latestDate,DateTime.Today);
            }
        }
    }
}