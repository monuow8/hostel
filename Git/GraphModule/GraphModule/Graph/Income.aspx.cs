﻿using GraphModule.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace GraphModule.Graph
{
    public partial class Income : System.Web.UI.Page
    {
        Font titleFont = new Font("Arial", 16, FontStyle.Bold);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                #region initial value
                if (ddlMonth.DataSource == null)
                {
                    ddlMonth.DataSource = DateCriteria.MonthList;
                    ddlMonth.DataTextField = "Text";
                    ddlMonth.DataValueField = "Value";
                    ddlMonth.DataBind();
                    ddlMonthCompare.DataSource = DateCriteria.MonthList;
                    ddlMonthCompare.DataTextField = "Text";
                    ddlMonthCompare.DataValueField = "Value";
                    ddlMonthCompare.DataBind();
                    ddlYear.DataSource = DateCriteria.YearList;
                    ddlYear.DataTextField = "Text";
                    ddlYear.DataValueField = "Text";
                    ddlYear.DataBind();
                    ddlYearCompare.DataSource = DateCriteria.YearList;
                    ddlYearCompare.DataTextField = "Text";
                    ddlYearCompare.DataValueField = "Text";
                    ddlYearCompare.DataBind();
                }
                #endregion
            }

            chartCtrl.Series.Clear();
            chartCtrl.Legends.Clear();
        }

        protected void btnViewGraph_Click(object sender, EventArgs e)
        {
            //SetSelection();

            GraphController controller = new GraphController();
            int cntSeries = 0;
            int year = Convert.ToInt32(ddlYear.SelectedValue);
            

            if (rdoMonth.Checked) // Monthly
            {
                lbTotal.Visible = false;
                lbTotalCompare.Visible = false;

                int month = Convert.ToInt32(ddlMonth.SelectedValue);
                cntSeries = GenerateMonthlyGraph(cntSeries, year, month);
                Session.Remove("GraphData");
                chartCtrl.Titles.Add(new Title("Income : " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(month))
    + " " + ddlYear.SelectedValue.ToString(), Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Date";
            }
            else // Yearly
            {
                cntSeries = GenerateYealyGraph(cntSeries, year);

                lbTotal.Visible = true;
                lbTotalCompare.Visible = false;

                Session.Remove("GraphData");
                chartCtrl.Titles.Add(new Title("Income : " + ddlYear.SelectedValue.ToString(), Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Months";
            }

            chartCtrl.ChartAreas["ChartArea"].AxisX.TitleFont = titleFont;
            chartCtrl.ChartAreas["ChartArea"].AxisY.Title = "Amount";
            chartCtrl.ChartAreas["ChartArea"].AxisY.TitleFont = titleFont;

            chartCtrl.Visible = true;
            ClearSelection();
        }

        protected void btnCompareGraph_Click(object sender, EventArgs e)
        {
            //SetSelection();

            int year = Convert.ToInt32(ddlYear.SelectedValue);
            int yearToCompare = Convert.ToInt32(ddlYearCompare.SelectedValue);
            int cntSeries = 0;

            if (rdoMonth.Checked) // Monthly
            {
                lbTotal.Visible = false;
                lbTotalCompare.Visible = false;

                int month = Convert.ToInt32(ddlMonth.SelectedValue);
                int monthToCompare = Convert.ToInt32(ddlMonthCompare.SelectedValue);

                cntSeries = GenerateMonthlyGraph(cntSeries, year, month);
                cntSeries = GenerateMonthlyGraph(cntSeries, yearToCompare, monthToCompare, true);

                chartCtrl.Titles.Add(new Title("Income : " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(month))
                    + " " + year + " compare with " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(monthToCompare))
                    + " " + yearToCompare, Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Dates";
            }
            else // Yearly
            {
                cntSeries = GenerateYealyGraph(cntSeries, year);
                cntSeries = GenerateYealyGraph(cntSeries, yearToCompare, true);

                lbTotal.Visible = true;
                lbTotalCompare.Visible = true;

                chartCtrl.Titles.Add(new Title("Income : " + year + " compare with " + yearToCompare, Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Months";
            }

            chartCtrl.ChartAreas["ChartArea"].AxisX.TitleFont = titleFont;
            chartCtrl.ChartAreas["ChartArea"].AxisY.Title = "Amount";
            chartCtrl.ChartAreas["ChartArea"].AxisY.TitleFont = titleFont;

            chartCtrl.Visible = true;
            ClearSelection();
        }

        private int GenerateYealyGraph(int cnt, int year, bool isCompare = false)
        {
            GraphController controller = new GraphController();
            IncomeYearlyData yearlyIncome = new IncomeYearlyData() { MonthList = new List<IncomeMonthSummary>() };

            #region loop get 12 months data
            for (int i = 1; i <= 12; i++)
            {
                DateTime from = new DateTime(year, i, 1);
                DateTime to = new DateTime(year, i, DateTime.DaysInMonth(year, i));
                List<IncomeMonthlyData> data = controller.GetIncomeMonthly(from, to);
                IncomeMonthSummary sum = new IncomeMonthSummary();
                int lastDayOfMonth = DateTime.DaysInMonth(year, i);
                sum.Month = i;
                if (data.Count > 0)
                {
                    foreach (IncomeMonthlyData d in data)
                    {
                        double nights = 0;
                        if (d.CheckIn.Month == i && d.CheckOut.Month == i) // check-in & check-out in same month
                        {
                            nights = (d.CheckOut - d.CheckIn).TotalDays;
                            sum.Income += Convert.ToInt32(nights * d.Price);
                        }
                        else if (d.CheckIn.Month == i && d.CheckOut.Month == i + 1) // checkout in next month
                        {
                            nights = ((new DateTime(year, i, lastDayOfMonth).AddDays(1)) - d.CheckIn).TotalDays;
                            sum.Income += Convert.ToInt32(nights * d.Price);
                        }
                        else if (d.CheckIn.Month == i - 1 && d.CheckOut.Month == i) // check-in since last month
                        {
                            nights = (d.CheckOut - new DateTime(year, i, 1)).TotalDays;
                            sum.Income += Convert.ToInt32(nights * d.Price);
                        }
                        else // rent everyday of month
                        {
                            int totalIncome = 0;
                            for (int j = 0; j < lastDayOfMonth; j++)
                            {
                                totalIncome += d.Price;
                            }
                            sum.Income += totalIncome;
                        }

                    }

                }
                else
                {
                    sum.Income += 0;
                }
                
                yearlyIncome.MonthList.Add(sum);
            }


            #endregion

            #region prepare values
            int yearlyAmount = 0;
            int[] values = new int[12]; ;
            string[] months = new string[12];
            for (int i = 0; i < 12; i++)
            {
                IncomeMonthSummary s = yearlyIncome.MonthList.Where(x => x.Month == (i + 1)).FirstOrDefault();
                months[i] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i + 1);
                values[i] = s.Income;
                yearlyAmount += s.Income;
            }
            #endregion

            #region add total 
            if (!isCompare)
            {
                lbTotal.Text = "Total income amount of " + year.ToString() + "  =  " + yearlyAmount.ToString();
            }
            else
            {
                lbTotalCompare.Text = "Total income amount of " + year.ToString() + "  =  " + yearlyAmount.ToString();
            }
            #endregion

            #region plot yearly graph
            chartCtrl.Legends.Add(new Legend(year.ToString().Substring(2, 2)));
            chartCtrl.Legends[year.ToString().Substring(2, 2)].Title = year.ToString();
            chartCtrl.Legends[year.ToString().Substring(2, 2)].DockedToChartArea = "ChartArea";

            chartCtrl.Series.Add(cnt.ToString());
            chartCtrl.Series[cnt.ToString()].Points.DataBindXY(months, values);
            chartCtrl.Series[cnt.ToString()].ChartType = SeriesChartType.Line;
            chartCtrl.Series[cnt.ToString()].Legend = year.ToString().Substring(2, 2);
            chartCtrl.Series[cnt.ToString()].IsVisibleInLegend = true;
            chartCtrl.Series[cnt.ToString()].IsValueShownAsLabel = true;
            chartCtrl.Series[cnt.ToString()]["ShowMarkerLines"] = "true";
            chartCtrl.Series[cnt.ToString()].EmptyPointStyle.MarkerColor = Color.Red;
            chartCtrl.Series[cnt.ToString()].EmptyPointStyle.MarkerSize = 15;
            chartCtrl.Series[cnt.ToString()].EmptyPointStyle.MarkerStyle = MarkerStyle.Cross;

            #region generate comparision %
            if (isCompare)
            {
                IncomeYearlyData compareYear = Session["GraphData"] as IncomeYearlyData;
                for (int i = 0; i < 12; i++)
                {
                    double income = yearlyIncome.MonthList[i].Income;
                    double compareIncome = compareYear.MonthList[i].Income;
                    TextAnnotation an = new TextAnnotation();

                    if (compareIncome != 0)
                    {
                        double percent = Math.Round((income - compareIncome) / Math.Abs(compareIncome) * 100, 2);

                        if (percent < 0)
                        {
                            an.Text = percent + "%";
                            an.ForeColor = Color.Red;
                        }
                        else
                        {
                            an.Text = "+" + percent + "%";
                            an.ForeColor = Color.Green;
                        }
                    }
                    else
                    {
                        if (income > 0)
                            an.Text = "+" + (income * 100) + "%";
                        else
                            an.Text = "+" + compareIncome + "%";
                        an.ForeColor = Color.Green;
                    }
                    chartCtrl.Annotations.Add(an);
                    chartCtrl.Annotations[i].AnchorDataPoint = chartCtrl.Series[cnt.ToString()].Points[i];
                }
                Session.Remove("GraphData");
            }
            #endregion

            Session["GraphData"] = yearlyIncome;

            return cnt + 1;
            #endregion
        }

        private int GenerateMonthlyGraph(int cnt, int year, int month, bool isCompare = false)
        {
            int lastDayOfMonth = DateTime.DaysInMonth(year, month);
            GraphController controller = new GraphController();
            DateTime from = new DateTime(year, month, 1);
            DateTime to = new DateTime(year, month, lastDayOfMonth);
            List<IncomeMonthlyData> data = controller.GetIncomeMonthly(from, to);

            if (data.Count > 0)
            {
                #region prepare values
                IncomeMonthlyData toDelete = new IncomeMonthlyData();
                int[] values = new int[lastDayOfMonth];
                string[] dates = new string[lastDayOfMonth];
                for (int i = 0; i < lastDayOfMonth; i++)
                {
                    dates[i] = (i + 1).ToString();
                    values[i] = 0;
                }
                // calculate income
                foreach (IncomeMonthlyData d in data)
                {
                    double nights = 0;

                    if (d.CheckIn.Month == month && d.CheckOut.Month == month) // check-in & check-out in same month
                    {
                        nights = (d.CheckOut - d.CheckIn).TotalDays;
                        values[d.CheckIn.Day - 1] += Convert.ToInt32(nights * d.Price);
                    }
                    else if (d.CheckIn.Month == month && d.CheckOut.Month == month + 1) // checkout in next month
                    {
                        nights = ((new DateTime(year, month, lastDayOfMonth).AddDays(1)) - d.CheckIn).TotalDays;
                        values[d.CheckIn.Day - 1] += Convert.ToInt32(nights * d.Price);
                    }
                    else if (d.CheckIn.Month == month - 1 && d.CheckOut.Month == month) // check-in since last month
                    {
                        nights = (d.CheckOut - new DateTime(year, month, 1)).TotalDays;
                        for (int i = 1; i <= nights; i++)
                        {
                            values[i] += d.Price;
                        }
                    }
                    else // rent everyday of month
                    {
                        for (int i = 0; i < lastDayOfMonth; i++)
                        {
                            values[i] += d.Price;
                        }
                    }
                }
                #endregion

                #region plot monthly graph
                chartCtrl.Legends.Add(new Legend(month + "'" + year.ToString().Substring(2, 2)));
                chartCtrl.Legends[month + "'" + year.ToString().Substring(2, 2)].Title
                    = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(month) + "'" + year.ToString().Substring(2, 2);
                chartCtrl.Legends[month + "'" + year.ToString().Substring(2, 2)].DockedToChartArea = "ChartArea";

                chartCtrl.Series.Add(cnt.ToString());
                chartCtrl.Series[cnt.ToString()].Points.DataBindXY(dates, values);
                chartCtrl.Series[cnt.ToString()].ChartType = SeriesChartType.Line;
                chartCtrl.Series[cnt.ToString()].Legend = month + "'" + year.ToString().Substring(2, 2);
                chartCtrl.Series[cnt.ToString()].IsVisibleInLegend = true;
                chartCtrl.Series[cnt.ToString()].IsValueShownAsLabel = true;
                chartCtrl.Series[cnt.ToString()]["ShowMarkerLines"] = "true";
                #endregion

                #region generate comparision %
                if (isCompare)
                {
                    int[] compareMonth = Session["GraphData"] as int[];
                    for (int i = 0; i < lastDayOfMonth; i++)
                    {
                        double income = values[i];
                        double compareIncome;
                        if (compareMonth.Count() > i)
                            compareIncome = compareMonth[i];
                        else
                            compareIncome = 0;

                        TextAnnotation an = new TextAnnotation();

                        if (compareIncome != 0)
                        {
                            double percent = Math.Round((income - compareIncome) / Math.Abs(compareIncome) * 100, 2);

                            if (percent < 0)
                            {
                                an.Text = percent + "%";
                                an.ForeColor = Color.Red;
                            }
                            else
                            {
                                an.Text = "+" + percent + "%";
                                an.ForeColor = Color.Green;
                            }
                        }
                        else
                        {
                            if (income > 0)
                                an.Text = "+" + (income * 100) + "%";
                            else
                                an.Text = "+" + compareIncome + "%";
                            an.ForeColor = Color.Green;
                        }
                        chartCtrl.Annotations.Add(an);
                        chartCtrl.Annotations[i].AnchorDataPoint = chartCtrl.Series[cnt.ToString()].Points[i];
                    }
                    Session.Remove("GraphData");
                }
                #endregion

                Session["GraphData"] = values;
            }
            return cnt + 1;
        }

        //private void SetSelection()
        //{
        //    FormSelection selection = new FormSelection()
        //    {
        //        Month = ddlMonth.SelectedIndex,
        //        MonthToCompare = ddlMonthCompare.SelectedIndex
        //        ,
        //        Year = ddlYear.SelectedIndex,
        //        YearToCompare = ddlYearCompare.SelectedIndex
        //        ,
        //        IsMonthly = rdoMonth.Checked
        //    };
        //    Session["LodgerSelection"] = selection;
        //}

        private void ClearSelection()
        {
            ddlMonth.SelectedIndex = 0;
            ddlMonthCompare.SelectedIndex = 0;
            ddlYear.SelectedIndex = 0;
            ddlYearCompare.SelectedIndex = 0;
            rdoMonth.Checked = true;

            if (chartCtrl.Series.Count > 0)
            {
                string name = "";
                for (int i = 0; i < chartCtrl.Series.Count; i++)
                {
                    chartCtrl.Series[i].Name = name;
                    name += " ";
                }
            }
        }
    }
}