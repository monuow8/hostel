﻿using GraphModule.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Drawing;

namespace GraphModule.Graph
{
    public partial class Nationality : System.Web.UI.Page
    {
        Font titleFont = new Font("Arial", 16, FontStyle.Bold);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                #region initial value
                if (ddlMonth.DataSource == null)
                {
                    ddlMonth.DataSource = DateCriteria.MonthList;
                    ddlMonth.DataTextField = "Text";
                    ddlMonth.DataValueField = "Value";
                    ddlMonth.DataBind();
                    ddlMonthCompare.DataSource = DateCriteria.MonthList;
                    ddlMonthCompare.DataTextField = "Text";
                    ddlMonthCompare.DataValueField = "Value";
                    ddlMonthCompare.DataBind();
                    ddlYear.DataSource = DateCriteria.YearList;
                    ddlYear.DataTextField = "Text";
                    ddlYear.DataValueField = "Text";
                    ddlYear.DataBind();
                    ddlYearCompare.DataSource = DateCriteria.YearList;
                    ddlYearCompare.DataTextField = "Text";
                    ddlYearCompare.DataValueField = "Text";
                    ddlYearCompare.DataBind();
                }
                #endregion
            }

            //if (Session["NationnalitySelection"] != null)
            //{
            //    NationalitySelection selection = Session["NationnalitySelection"] as NationalitySelection;
            //    ddlMonth.SelectedIndex = selection.Month;
            //    ddlMonthCompare.SelectedIndex = selection.MonthToCompare;
            //    ddlYear.SelectedIndex = selection.Year;
            //    ddlYearCompare.SelectedIndex = selection.YearToCompare;
            //    rdoMonth.Checked = selection.IsMonthly;
            //    Session.Remove("NationnalitySelection");
            //}
            chartCtrl.Series.Clear();
            chartCtrl.Legends.Clear();
        }

        protected void btnViewGraph_Click(object sender, EventArgs e)
        {
            //SetSelection();

            GraphController controller = new GraphController();
            int cntSeries = 0;
            int year = Convert.ToInt32(ddlYear.SelectedValue);

            if (rdoMonth.Checked) // Monthly
            {
                totalPanel.Visible = false;
                totalComparePanel.Visible = false;

                int month = Convert.ToInt32(ddlMonth.SelectedValue);
                cntSeries = GenerateMonthlyGraph(cntSeries,year, month);
                Session.Remove("GraphData");
                chartCtrl.Titles.Add(new Title("Nationality : " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(month))
    + " " + ddlYear.SelectedValue.ToString(), Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Dates";
            }
            else // Yearly
            {
                totalPanel.Visible = true;
                lbTotal.Items.Clear();
                totalComparePanel.Visible = false;

                cntSeries = GenerateYealyGraph(cntSeries, year);

                chartCtrl.Titles.Add(new Title("Nationality : " + ddlYear.SelectedValue.ToString(), Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Months";
            }

            chartCtrl.ChartAreas["ChartArea"].AxisX.TitleFont = titleFont;
            chartCtrl.ChartAreas["ChartArea"].AxisY.Title = "Quantity";
            chartCtrl.ChartAreas["ChartArea"].AxisY.TitleFont = titleFont;

            chartCtrl.Visible = true;
            ClearSelection();
        }

        protected void btnCompareGraph_Click(object sender, EventArgs e)
        {
            //SetSelection();

            int year = Convert.ToInt32(ddlYear.SelectedValue);
            int yearToCompare = Convert.ToInt32(ddlYearCompare.SelectedValue);
            int cntSeries = 0;

            if (rdoMonth.Checked) // Monthly
            {
                totalPanel.Visible = false;
                totalComparePanel.Visible = false;

                int month = Convert.ToInt32(ddlMonth.SelectedValue);
                int monthToCompare = Convert.ToInt32(ddlMonthCompare.SelectedValue);

                cntSeries = GenerateMonthlyGraph(cntSeries,year, month);
                cntSeries = GenerateMonthlyGraph(cntSeries,yearToCompare, monthToCompare, true);

                chartCtrl.Titles.Add(new Title("Nationality : " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(month))
                    + " " + year + " compare with " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(monthToCompare))
                    + " " + yearToCompare, Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Dates";
            }
            else // Yearly
            {
                totalPanel.Visible = true;
                lbTotal.Items.Clear();
                totalComparePanel.Visible = true;
                lbTotalCompare.Items.Clear();

                cntSeries = GenerateYealyGraph(cntSeries, year);
                cntSeries = GenerateYealyGraph(cntSeries, yearToCompare, true);

                chartCtrl.Titles.Add(new Title("Nationality : " + year + " compare with " + yearToCompare, Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Months";
            }

            chartCtrl.ChartAreas["ChartArea"].AxisX.TitleFont = titleFont;
            chartCtrl.ChartAreas["ChartArea"].AxisY.Title = "Quantity";
            chartCtrl.ChartAreas["ChartArea"].AxisY.TitleFont = titleFont;

            chartCtrl.Visible = true;
            ClearSelection();
        }

        private int GenerateYealyGraph(int cnt, int year, bool isCompare = false)
        {
            GraphController controller = new GraphController();
            List<NationalityMonthSummary> sumList = new List<NationalityMonthSummary>();
            for (int i = 1; i <= 12; i++) // loop get 12 months data
            {
                DateTime from = new DateTime(year, i, 1);
                DateTime to = new DateTime(year, i, DateTime.DaysInMonth(year, i));
                List<NationalityMonthlyData> data = controller.GetNationalityMonthly(from, to);
                if (data.Count > 0)
                {
                    foreach (NationalityMonthlyData d in data)
                    {
                        #region summarize each month
                        NationalityMonthSummary sum = new NationalityMonthSummary();
                        int qty = 0;
                        qty += d.Day1;
                        qty += d.Day2;
                        qty += d.Day3;
                        qty += d.Day4;
                        qty += d.Day5;
                        qty += d.Day6;
                        qty += d.Day7;
                        qty += d.Day8;
                        qty += d.Day9;
                        qty += d.Day10;
                        qty += d.Day11;
                        qty += d.Day12;
                        qty += d.Day13;
                        qty += d.Day14;
                        qty += d.Day15;
                        qty += d.Day16;
                        qty += d.Day17;
                        qty += d.Day18;
                        qty += d.Day19;
                        qty += d.Day20;
                        qty += d.Day21;
                        qty += d.Day22;
                        qty += d.Day23;
                        qty += d.Day24;
                        qty += d.Day25;
                        qty += d.Day26;
                        qty += d.Day27;
                        qty += d.Day28;
                        qty += d.Day29;
                        qty += d.Day30;
                        qty += d.Day31;

                        sum.Month = i;
                        sum.Quantity = qty;
                        sum.Nationality = d.Nationality;
                        #endregion
                        sumList.Add(sum);
                    }
                }
            } // end loop get 12 months data

            if (sumList.Count > 0)
            {
                #region add each nationality to list
                List<NationalityYearlyData> list = new List<NationalityYearlyData>();
                foreach (NationalityMonthSummary s in sumList)
                {
                    bool added = false;
                    List<NationalityYearlyData> currentList = list;
                    for (int i = 0; i < currentList.Count && !added; i++)
                    {
                        if (currentList[i].Nationality == s.Nationality)
                        {
                            list[i].NationalityMonthSummaryList.Add(s);
                            added = true;
                        }
                    }
                    if (!added) // new Nationality
                    {
                        NationalityYearlyData newNationality = new NationalityYearlyData()
                        {
                            Nationality = s.Nationality,
                            NationalityMonthSummaryList = new List<NationalityMonthSummary>()
                        };
                        newNationality.NationalityMonthSummaryList.Add(s);
                        list.Add(newNationality);
                    }
                }
                #endregion

                #region plot each series
                int[] values = new int[12]; ;
                string[] months = new string[12];
                List<NationalityCompareData> compareList = new List<NationalityCompareData>();
                List<NationalityCompareData> refList = new List<NationalityCompareData>();
                int anCnt = 0;
                foreach (NationalityYearlyData d in list)
                {
                    #region prepare values
                    int yearlyQty = 0;
                    values = new int[12];
                    months = new string[12];
                    for (int i = 0; i < 12; i++)
                    {
                        NationalityMonthSummary s = d.NationalityMonthSummaryList.Where(x => x.Nationality == d.Nationality && x.Month == (i + 1)).FirstOrDefault();
                        months[i] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i + 1);
                        if (s != null)
                            values[i] = s.Quantity;
                        else
                            values[i] = 0;

                        yearlyQty += values[i];
                    }
                    #endregion

                    #region add total
                    if (!isCompare)
                    {
                        lbTotal.Items.Add("Quantity of " + d.Nationality + "  =  " + yearlyQty.ToString());
                    }
                    else
                    {
                        lbTotalCompare.Items.Add("Quantity of " + d.Nationality + "  =  " + yearlyQty.ToString());
                    }
                    #endregion

                    #region plot yearly graph
                    chartCtrl.Legends.Add(new Legend(d.Nationality + year.ToString().Substring(2, 2)));
                    chartCtrl.Legends[d.Nationality + year.ToString().Substring(2, 2)].Title = d.Nationality + "'" + year.ToString().Substring(2, 2);
                    chartCtrl.Legends[d.Nationality + year.ToString().Substring(2, 2)].DockedToChartArea = "ChartArea";

                    chartCtrl.Series.Add(cnt.ToString());
                    chartCtrl.Series[cnt.ToString()].Points.DataBindXY(months, values);
                    chartCtrl.Series[cnt.ToString()].ChartType = SeriesChartType.Column;
                    chartCtrl.Series[cnt.ToString()].Legend = d.Nationality + year.ToString().Substring(2, 2);
                    chartCtrl.Series[cnt.ToString()].IsVisibleInLegend = true;
                    chartCtrl.Series[cnt.ToString()].IsValueShownAsLabel = true;
                    #endregion

                    compareList.Add(new NationalityCompareData() { Nationality = d.Nationality, Values = values });

                    #region generate comparision %
                    if (isCompare)
                    {
                        if (refList.Count == 0) refList = Session["GraphData"] as List<NationalityCompareData>;

                        NationalityCompareData refItem = refList.Where(x => x.Nationality == d.Nationality).FirstOrDefault();
                        if (refItem != null) // check Nationality first
                        {
                            for (int i = 0; i < 12; i++)
                            {
                                double qty = values[i];
                                double compareQty = refItem.Values[i];
                                TextAnnotation an = new TextAnnotation();

                                if (compareQty != 0)
                                {
                                    double percent = Math.Round((qty - compareQty) / Math.Abs(compareQty) * 100, 2);

                                    if (percent < 0)
                                    {
                                        an.Text = percent + "%";
                                        an.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        an.Text = "+" + percent + "%";
                                        an.ForeColor = Color.Green;
                                    }
                                }
                                else
                                {
                                    if (qty > 0)
                                        an.Text = "+" + (qty * 100) + "%";
                                    else
                                        an.Text = "+" + compareQty + "%";
                                    an.ForeColor = Color.Green;
                                }
                                an.AnchorOffsetY = 3.5;
                                chartCtrl.Annotations.Add(an);
                                chartCtrl.Annotations[anCnt].AnchorDataPoint = chartCtrl.Series[cnt.ToString()].Points[i];
                                anCnt++;
                            }
                        }
                        Session.Remove("GraphData");
                    }
                    #endregion
                    cnt++;
                }
                #endregion
                Session["GraphData"] = compareList;
                if (!isCompare)
                {
                    lbYear.Text = "Nationality of " + year.ToString() + " : ";
                }
                else
                {
                    lbYearCompare.Text = "Nationality of " + year.ToString() + " : ";
                }
            }
            chartCtrl.Width = new Unit(cnt * 350);
            return cnt;
        }

        private int GenerateMonthlyGraph(int cnt, int year, int month, bool isCompare = false)
        {
            int lastDayOfMonth = DateTime.DaysInMonth(year, month);
            GraphController controller = new GraphController();
            DateTime from = new DateTime(year, month, 1);
            DateTime to = new DateTime(year, month, lastDayOfMonth);
            List<NationalityMonthlyData> data = controller.GetNationalityMonthly(from, to);

            if (data.Count > 0)
            {
                int[] values = new int[lastDayOfMonth]; ;
                string[] dates = new string[lastDayOfMonth];

                List<NationalityCompareData> compareList = new List<NationalityCompareData>();
                List<NationalityCompareData> refList = new List<NationalityCompareData>();
                int anCnt = 0;

                foreach (NationalityMonthlyData d in data)
                {
                    #region prepare values
                    values = new int[lastDayOfMonth];
                    dates = new string[lastDayOfMonth];
                    for (int i = 0; i < lastDayOfMonth; i++)
                    {
                        dates[i] = (i + 1).ToString();
                    }
                    values[0] = d.Day1;
                    values[1] = d.Day2;
                    values[2] = d.Day3;
                    values[3] = d.Day4;
                    values[4] = d.Day5;
                    values[5] = d.Day6;
                    values[6] = d.Day7;
                    values[7] = d.Day8;
                    values[8] = d.Day9;
                    values[9] = d.Day10;
                    values[10] = d.Day11;
                    values[11] = d.Day12;
                    values[12] = d.Day13;
                    values[13] = d.Day14;
                    values[14] = d.Day15;
                    values[15] = d.Day16;
                    values[16] = d.Day17;
                    values[17] = d.Day18;
                    values[18] = d.Day19;
                    values[19] = d.Day20;
                    values[20] = d.Day21;
                    values[21] = d.Day22;
                    values[22] = d.Day23;
                    values[23] = d.Day24;
                    values[24] = d.Day25;
                    values[25] = d.Day26;
                    values[26] = d.Day27;
                    values[27] = d.Day28;
                    if (lastDayOfMonth >= 29)
                        values[28] = d.Day29;
                    if (lastDayOfMonth >= 30)
                        values[29] = d.Day30;
                    if (lastDayOfMonth == 31)
                        values[30] = d.Day31;
                    #endregion

                    #region plot monthly graph
                    chartCtrl.Legends.Add(new Legend(d.Nationality + " " + month + "'" + year.ToString().Substring(2, 2)));
                    chartCtrl.Legends[d.Nationality + " " + month + "'" + year.ToString().Substring(2, 2)].Title
                        = d.Nationality + " " + CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(month) + "'" + year.ToString().Substring(2, 2);
                    chartCtrl.Legends[d.Nationality + " " + month + "'" + year.ToString().Substring(2, 2)].DockedToChartArea = "ChartArea";

                    chartCtrl.Series.Add(cnt.ToString());
                    chartCtrl.Series[cnt.ToString()].Points.DataBindXY(dates, values);
                    chartCtrl.Series[cnt.ToString()].ChartType = SeriesChartType.Column;
                    chartCtrl.Series[cnt.ToString()].Legend = d.Nationality + " " + month + "'" + year.ToString().Substring(2, 2);
                    chartCtrl.Series[cnt.ToString()].IsVisibleInLegend = true;
                    chartCtrl.Series[cnt.ToString()].IsValueShownAsLabel = true;
                    //chartCtrl.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, cnt.ToString());
                    #endregion

                    compareList.Add(new NationalityCompareData() { Nationality = d.Nationality, Values = values });

                    #region generate comparision %
                    if (isCompare)
                    {
                        if (refList.Count == 0) refList = Session["GraphData"] as List<NationalityCompareData>;

                        NationalityCompareData refItem = refList.Where(x => x.Nationality == d.Nationality).FirstOrDefault();
                        if (refItem != null) // check Nationality first
                        {
                            for (int i = 0; i < lastDayOfMonth; i++)
                            {
                                double qty = values[i];
                                double compareQty;
                                if (refItem.Values.Count() > i)
                                    compareQty = refItem.Values[i];
                                else
                                    compareQty = 0;

                                TextAnnotation an = new TextAnnotation();

                                if (compareQty != 0)
                                {
                                    double percent = Math.Round((qty - compareQty) / Math.Abs(compareQty) * 100, 2);

                                    if (percent < 0)
                                    {
                                        an.Text = percent + "%";
                                        an.ForeColor = Color.Red;
                                    }
                                    else
                                    {
                                        an.Text = "+" + percent + "%";
                                        an.ForeColor = Color.Green;
                                    }
                                }
                                else
                                {
                                    if (qty > 0)
                                        an.Text = "+" + (qty * 100) + "%";
                                    else
                                        an.Text = "+" + compareQty + "%";
                                    an.ForeColor = Color.Green;
                                }
                                an.AnchorOffsetY = 3.5;
                                chartCtrl.Annotations.Add(an);
                                chartCtrl.Annotations[anCnt].AnchorDataPoint = chartCtrl.Series[cnt.ToString()].Points[i];
                                anCnt++;
                            }
                        }
                        Session.Remove("GraphData");
                    }
                    #endregion
                    cnt++;
                }
                Session["GraphData"] = compareList;
            }
            chartCtrl.Width = new Unit(cnt * 350);
            return cnt;
        }

        //private void SetSelection()
        //{
        //    FormSelection selection = new FormSelection()
        //    {
        //        Month = ddlMonth.SelectedIndex,
        //        MonthToCompare = ddlMonthCompare.SelectedIndex
        //        ,
        //        Year = ddlYear.SelectedIndex,
        //        YearToCompare = ddlYearCompare.SelectedIndex
        //        ,
        //        IsMonthly = rdoMonth.Checked
        //    };
        //    Session["NationnalitySelection"] = selection;
        //}

        private void ClearSelection()
        {
            ddlMonth.SelectedIndex = 0;
            ddlMonthCompare.SelectedIndex = 0;
            ddlYear.SelectedIndex = 0;
            ddlYearCompare.SelectedIndex = 0;
            rdoMonth.Checked = true;

            if (chartCtrl.Series.Count > 0)
            {
                string name = "";
                for (int i = 0; i < chartCtrl.Series.Count; i++)
                {
                    chartCtrl.Series[i].Name = name;
                    name += " ";
                }
            }
        }
    }
}