﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using GraphModule.Model;
using System.Drawing;

namespace GraphModule.Graph
{
    public partial class Lodger : System.Web.UI.Page
    {
        Font titleFont = new Font("Arial", 16, FontStyle.Bold);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                #region initial value
                if (ddlMonth.DataSource == null)
                {
                    ddlMonth.DataSource = DateCriteria.MonthList;
                    ddlMonth.DataTextField = "Text";
                    ddlMonth.DataValueField = "Value";
                    ddlMonth.DataBind();
                    ddlMonthCompare.DataSource = DateCriteria.MonthList;
                    ddlMonthCompare.DataTextField = "Text";
                    ddlMonthCompare.DataValueField = "Value";
                    ddlMonthCompare.DataBind();
                    ddlYear.DataSource = DateCriteria.YearList;
                    ddlYear.DataTextField = "Text";
                    ddlYear.DataValueField = "Text";
                    ddlYear.DataBind();
                    ddlYearCompare.DataSource = DateCriteria.YearList;
                    ddlYearCompare.DataTextField = "Text";
                    ddlYearCompare.DataValueField = "Text";
                    ddlYearCompare.DataBind();
                }
                #endregion
            }

            chartCtrl.Series.Clear();
            chartCtrl.Legends.Clear();
        }

        protected void btnViewGraph_Click(object sender, EventArgs e)
        {
            //SetSelection();

            GraphController controller = new GraphController();
            int cntSeries = 0;
            int year = Convert.ToInt32(ddlYear.SelectedValue);

            if (rdoMonth.Checked) // Monthly
            {
                lbTotal.Visible = false;
                lbTotalCompare.Visible = false;

                int month = Convert.ToInt32(ddlMonth.SelectedValue);
                cntSeries = GenerateMonthlyGraph(cntSeries, year, month);
                chartCtrl.Titles.Add(new Title("Lodger : " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(month))
    + " " + ddlYear.SelectedValue.ToString(), Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Dates";
            }
            else // Yearly
            {
                cntSeries = GenerateYealyGraph(cntSeries, year);

                lbTotal.Visible = true;
                lbTotalCompare.Visible = false;

                chartCtrl.Titles.Add(new Title("Lodger : " + ddlYear.SelectedValue.ToString(), Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Months";
            }

            chartCtrl.ChartAreas["ChartArea"].AxisX.TitleFont = titleFont;
            chartCtrl.ChartAreas["ChartArea"].AxisY.Title = "Quantity";
            chartCtrl.ChartAreas["ChartArea"].AxisY.TitleFont = titleFont;

            chartCtrl.Visible = true;
        }

        protected void btnCompareGraph_Click(object sender, EventArgs e)
        {
            //SetSelection();

            int year = Convert.ToInt32(ddlYear.SelectedValue);
            int yearToCompare = Convert.ToInt32(ddlYearCompare.SelectedValue);
            int cntSeries = 0;

            if (rdoMonth.Checked) // Monthly
            {
                lbTotal.Visible = false;
                lbTotalCompare.Visible = false;

                int month = Convert.ToInt32(ddlMonth.SelectedValue);
                int monthToCompare = Convert.ToInt32(ddlMonthCompare.SelectedValue);

                cntSeries = GenerateMonthlyGraph(cntSeries, year, month);
                cntSeries = GenerateMonthlyGraph(cntSeries, yearToCompare, monthToCompare,true);

                chartCtrl.Titles.Add(new Title("Lodger : " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(month))
                    + " " + year + " compare with " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(monthToCompare))
                    + " " + yearToCompare, Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Dates";
            }
            else // Yearly
            {
                cntSeries = GenerateYealyGraph(cntSeries, year);
                cntSeries = GenerateYealyGraph(cntSeries, yearToCompare,true);

                lbTotal.Visible = true;
                lbTotalCompare.Visible = true;

                chartCtrl.Titles.Add(new Title("Lodger : " + year + " compare with " + yearToCompare, Docking.Top, titleFont, Color.Black));
                chartCtrl.ChartAreas["ChartArea"].AxisX.Title = "Months";
            }

            chartCtrl.ChartAreas["ChartArea"].AxisX.TitleFont = titleFont;
            chartCtrl.ChartAreas["ChartArea"].AxisY.Title = "Quantity";
            chartCtrl.ChartAreas["ChartArea"].AxisY.TitleFont = titleFont;

            chartCtrl.Visible = true;
            ClearSelection();
        }

        private int GenerateYealyGraph(int cnt, int year,bool isCompare = false)
        {
            GraphController controller = new GraphController();
            LodgerYearlyData yearlyLodger = new LodgerYearlyData() { MonthList = new List<LodgerMonthSummary>() };

            #region loop get 12 months data
            for (int i = 1; i <= 12; i++)
            {
                DateTime from = new DateTime(year, i, 1);
                DateTime to = new DateTime(year, i, DateTime.DaysInMonth(year, i));
                List<LodgerMonthlyData> data = controller.GetLodgerMonthly(from, to);
                LodgerMonthSummary sum = new LodgerMonthSummary();
                sum.Month = i;
                if (data.Count > 0)
                {
                    foreach (LodgerMonthlyData d in data)
                    {
                        sum.Quantity += d.Qty;
                    }
                }
                else
                {
                    sum.Quantity = 0;
                }
                yearlyLodger.MonthList.Add(sum);
            }
            #endregion

            #region prepare values
            int yearlyQty = 0;
            int[] values = new int[12]; ;
            string[] months = new string[12];
            for (int i = 0; i < 12; i++)
            {
                LodgerMonthSummary s = yearlyLodger.MonthList.Where(x => x.Month == (i + 1)).FirstOrDefault();
                months[i] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i + 1);
                if (s != null)
                    values[i] = s.Quantity;
                else
                    values[i] = 0;

                yearlyQty += values[i];
            }
            #endregion

            #region add total
            if (!isCompare)
            {
                lbTotal.Text = "Total lodger quantity of " + year.ToString() + "  =  " + yearlyQty.ToString();
            }
            else
            {
                lbTotalCompare.Text = "Total lodger quantity of " + year.ToString() + "  =  " + yearlyQty.ToString();
            }
            #endregion

            #region plot yearly graph
            chartCtrl.Legends.Add(new Legend(year.ToString().Substring(2, 2)));
            chartCtrl.Legends[year.ToString().Substring(2, 2)].Title = year.ToString();
            chartCtrl.Legends[year.ToString().Substring(2, 2)].DockedToChartArea = "ChartArea";

            chartCtrl.Series.Add(cnt.ToString());
            chartCtrl.Series[cnt.ToString()].Points.DataBindXY(months, values);
            chartCtrl.Series[cnt.ToString()].ChartType = SeriesChartType.Line;
            chartCtrl.Series[cnt.ToString()].Legend = year.ToString().Substring(2, 2);
            chartCtrl.Series[cnt.ToString()].IsVisibleInLegend = true;
            chartCtrl.Series[cnt.ToString()].IsValueShownAsLabel = true;
            chartCtrl.Series[cnt.ToString()]["ShowMarkerLines"] = "true";


            #region generate comparision %
            if (isCompare)
            {
                LodgerYearlyData compareYear = Session["GraphData"] as LodgerYearlyData;
                for (int i = 0; i < 12; i++)
                {
                    double qty = yearlyLodger.MonthList[i].Quantity;
                    double compareQty = compareYear.MonthList[i].Quantity;
                    TextAnnotation an = new TextAnnotation();

                    if (compareQty != 0)
                    {
                        double percent = Math.Round((qty - compareQty) / Math.Abs(compareQty) * 100, 2);

                        if (percent < 0)
                        {
                            an.Text = percent + "%";
                            an.ForeColor = Color.Red;
                        }
                        else
                        {
                            an.Text = "+" + percent + "%";
                            an.ForeColor = Color.Green;
                        }
                    }
                    else
                    {
                        if (qty > 0)
                            an.Text = "+" + (qty * 100) + "%";
                        else
                            an.Text = "+" + compareQty + "%";
                        an.ForeColor = Color.Green;
                    }
                    chartCtrl.Annotations.Add(an);
                    chartCtrl.Annotations[i].AnchorDataPoint = chartCtrl.Series[cnt.ToString()].Points[i];
                }
                Session.Remove("GraphData");
            }
            #endregion

            Session["GraphData"] = yearlyLodger;

            return cnt + 1;
            #endregion
        }

        private int GenerateMonthlyGraph(int cnt, int year, int month,bool isCompare = false)
        {
            int lastDayOfMonth = DateTime.DaysInMonth(year, month);
            GraphController controller = new GraphController();
            DateTime from = new DateTime(year, month, 1);
            DateTime to = new DateTime(year, month, lastDayOfMonth);
            List<LodgerMonthlyData> data = controller.GetLodgerMonthly(from, to);

            if (data.Count > 0)
            {
                #region prepare values
                bool added = false;
                LodgerMonthlyData toDelete = new LodgerMonthlyData();
                int[] values = new int[lastDayOfMonth]; ;
                string[] dates = new string[lastDayOfMonth];
                for (int i = 0; i < lastDayOfMonth; i++)
                {
                    foreach (LodgerMonthlyData d in data)
                    {
                        if (d.CheckIn == new DateTime(year, month, (i + 1)))
                        {
                            values[i] = d.Qty;
                            dates[i] = (i+1).ToString();
                            added = true;
                            toDelete = d;
                            break;
                        }
                    }
                    if (!added)
                    {
                        values[i] = 0;
                        dates[i] = (i + 1).ToString();
                    }
                    else
                    {
                        data.Remove(toDelete);
                        added = false;
                    }
                }

                #endregion

                #region plot monthly graph
                chartCtrl.Legends.Add(new Legend(month + "'" + year.ToString().Substring(2, 2)));
                chartCtrl.Legends[month + "'" + year.ToString().Substring(2, 2)].Title
                    = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(month) + "'" + year.ToString().Substring(2, 2);
                chartCtrl.Legends[month + "'" + year.ToString().Substring(2, 2)].DockedToChartArea = "ChartArea";

                chartCtrl.Series.Add(cnt.ToString());
                chartCtrl.Series[cnt.ToString()].Points.DataBindXY(dates, values);
                chartCtrl.Series[cnt.ToString()].ChartType = SeriesChartType.Line;
                chartCtrl.Series[cnt.ToString()].Legend = month + "'" + year.ToString().Substring(2, 2);
                chartCtrl.Series[cnt.ToString()].IsVisibleInLegend = true;
                chartCtrl.Series[cnt.ToString()].IsValueShownAsLabel = true;
                chartCtrl.Series[cnt.ToString()]["ShowMarkerLines"] = "true";
                #endregion

                #region generate comparision %
                if (isCompare)
                {
                    int[] compareMonth = Session["GraphData"] as int[];
                    for (int i = 0; i < lastDayOfMonth; i++)
                    {
                        double qty = values[i];
                        double compareQty;
                        if (compareMonth.Count() > i)
                            compareQty = compareMonth[i];
                        else
                            compareQty = 0;

                        TextAnnotation an = new TextAnnotation();

                        if (compareQty != 0)
                        {
                            double percent = Math.Round((qty - compareQty) / Math.Abs(compareQty) * 100, 2);

                            if (percent < 0)
                            {
                                an.Text = percent + "%";
                                an.ForeColor = Color.Red;
                            }
                            else
                            {
                                an.Text = "+" + percent + "%";
                                an.ForeColor = Color.Green;
                            }
                        }
                        else
                        {
                            if (qty > 0)
                                an.Text = "+" + (qty * 100) + "%";
                            else
                                an.Text = "+" + compareQty + "%";
                            an.ForeColor = Color.Green;
                        }
                        chartCtrl.Annotations.Add(an);
                        chartCtrl.Annotations[i].AnchorDataPoint = chartCtrl.Series[cnt.ToString()].Points[i];
                    }
                    Session.Remove("GraphData");
                }
                #endregion

                Session["GraphData"] = values;
            }
            return cnt + 1;
        }

        //private void SetSelection()
        //{
        //    FormSelection selection = new FormSelection()
        //    {
        //        Month = ddlMonth.SelectedIndex,
        //        MonthToCompare = ddlMonthCompare.SelectedIndex
        //        ,
        //        Year = ddlYear.SelectedIndex,
        //        YearToCompare = ddlYearCompare.SelectedIndex
        //        ,
        //        IsMonthly = rdoMonth.Checked
        //    };
        //    Session["LodgerSelection"] = selection;
        //}

        private void ClearSelection()
        {
            ddlMonth.SelectedIndex = 0;
            ddlMonthCompare.SelectedIndex = 0;
            ddlYear.SelectedIndex = 0;
            ddlYearCompare.SelectedIndex = 0;
            rdoMonth.Checked = true;

            if (chartCtrl.Series.Count > 0)
            {
                string name = "";
                for (int i = 0; i < chartCtrl.Series.Count; i++)
                {
                    chartCtrl.Series[i].Name = name;
                    name += " ";
                }
            }
        }
    }
}