﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Income.aspx.cs" Inherits="GraphModule.Graph.Income" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Income Summary</title>
    <link href="../Content/site.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.9.1.min.js"></script>
</head>

<script type="text/javascript">
    //$(document).ready(function () {
    //    $("#rdoMonth").click(function () {
    //        $("#ddlMonth").show();
    //        $("#ddlMonthCompare").show();
    //        $("#td-txt1").text("Month : ");
    //        $("#td-txt2").text("Compare to Month : ");
    //    });

    //    $("#rdoYear").click(function () {
    //        $("#ddlMonth").hide();
    //        $("#ddlMonthCompare").hide();
    //        $("#td-txt1").text("Year : ");
    //        $("#td-txt2").text("Compare to Year : ");
    //    });
    //});

    function ValidateField() {
        //if ($("#rdoMonth").is("Checked")) {
        //    if ($("#ddlMonth option:selected").text() == $("#ddlMonthCompare option:selected").text()
        //        && $("#ddlYear option:selected").text() == $("#ddlYearCompare option:selected").text()) {
        //        alert('month ?');
        //        return false;
        //    }
        //} else {
        //    if ($("#ddlYear option:selected").text() == $("#ddlYearCompare option:selected").text()) {
        //        alert('year ?')
        //        return false;
        //    }
        //}
    }

</script>

<body>
    <form id="Form1" runat="server" onsubmit="return ValidateField();">
        <div id="CriteriaDiv">
            <fieldset>
                <legend>Summarize By
                </legend>
                <asp:RadioButton ID="rdoMonth" GroupName="SearchTypeGroup" runat="server" Checked="true" Text="Monthly" />
                <asp:RadioButton ID="rdoYear" GroupName="SearchTypeGroup" runat="server" Text="Yearly" />

                <table>
                    <tr>
                        <td></td>
                        <td>
                            <asp:DropDownList ID="ddlMonth" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="ddlYear" runat="server"></asp:DropDownList>
                        </td>
                        <td>Compare to
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlMonthCompare" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="ddlYearCompare" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: right;">
                            <asp:Button ID="btnCompareGraph" runat="server" Text="Compare" OnClick="btnCompareGraph_Click" OnClientClick="ValidateField();" />
                            <asp:Button ID="btnViewGraph" runat="server" Text="View Summary" OnClick="btnViewGraph_Click" OnClientClick="ValidateField();" />
                        </td>
                    </tr>
                </table>

            </fieldset>
        </div>

        <div class="left-chart-div">
            <asp:Chart ID="chartCtrl" runat="server" Height="700px" Width="1600px">
                <Series>
                    <asp:Series Name="Series1" ChartArea="ChartArea">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            <asp:Panel ID="notFoundPanel" runat="server" Visible="false">
                <div class="data-not-found">
                    Data not found
                </div>
            </asp:Panel>
            <div>
                <asp:Label ID="lbTotal" runat="server" Visible="False" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" />
            </div>
            <div>
                <asp:Label ID="lbTotalCompare" runat="server" Visible="false" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" />
            </div>
        </div>
    </form>
</body>
</html>


