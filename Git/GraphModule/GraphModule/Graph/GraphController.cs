﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using GraphModule.Model;
using System.Globalization;

namespace GraphModule.Graph
{
    public class GraphController
    {
        private static SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader reader;

        public GraphController()
        {
            conn = new SqlConnection("Server=localhost;Database=Senior;Uid=sa;Pwd=sa;");
        }

        public List<GraphData> GetGraphData(DateTime from, DateTime to)
        {
            List<GraphData> list = new List<GraphData>();
            cmd = new SqlCommand("GetGraphData", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", from);
            cmd.Parameters.AddWithValue("toDate", to);

            conn.Open();
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                GraphData item = new GraphData();
                item.Nationality = reader["Nationality"].ToString();
                item.RoomType = reader["RoomType"].ToString();
                item.CheckIn = Convert.ToDateTime(reader["CheckIn"]);
                item.CheckOut = Convert.ToDateTime(reader["CheckOut"]);
                item.Price = Convert.ToInt32(reader["Price"]);
                list.Add(item);
            }

            conn.Close();
            return list;
        }

        public List<NationalityMonthlyData> GetNationalityMonthly(DateTime from, DateTime to)
        {
            List<NationalityMonthlyData> list = new List<NationalityMonthlyData>();
            cmd = new SqlCommand("GetNationalityMonthly", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", from);
            cmd.Parameters.AddWithValue("toDate", to);

            conn.Open();
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                #region transform object
                NationalityMonthlyData item = new NationalityMonthlyData();
                item.Nationality = reader["Nationality"].ToString();
                item.Day1 = Convert.ToInt32(reader["Day1"]);
                item.Day2 = Convert.ToInt32(reader["Day2"]);
                item.Day3 = Convert.ToInt32(reader["Day3"]);
                item.Day4 = Convert.ToInt32(reader["Day4"]);
                item.Day5 = Convert.ToInt32(reader["Day5"]);
                item.Day6 = Convert.ToInt32(reader["Day6"]);
                item.Day7 = Convert.ToInt32(reader["Day7"]);
                item.Day8 = Convert.ToInt32(reader["Day8"]);
                item.Day9 = Convert.ToInt32(reader["Day9"]);
                item.Day10 = Convert.ToInt32(reader["Day10"]);
                item.Day11 = Convert.ToInt32(reader["Day11"]);
                item.Day12 = Convert.ToInt32(reader["Day12"]);
                item.Day13 = Convert.ToInt32(reader["Day13"]);
                item.Day14 = Convert.ToInt32(reader["Day14"]);
                item.Day15 = Convert.ToInt32(reader["Day15"]);
                item.Day16 = Convert.ToInt32(reader["Day16"]);
                item.Day17 = Convert.ToInt32(reader["Day17"]);
                item.Day18 = Convert.ToInt32(reader["Day18"]);
                item.Day19 = Convert.ToInt32(reader["Day19"]);
                item.Day20 = Convert.ToInt32(reader["Day20"]);
                item.Day21 = Convert.ToInt32(reader["Day21"]);
                item.Day22 = Convert.ToInt32(reader["Day22"]);
                item.Day23 = Convert.ToInt32(reader["Day23"]);
                item.Day24 = Convert.ToInt32(reader["Day24"]);
                item.Day25 = Convert.ToInt32(reader["Day25"]);
                item.Day26 = Convert.ToInt32(reader["Day26"]);
                item.Day27 = Convert.ToInt32(reader["Day27"]);
                item.Day28 = Convert.ToInt32(reader["Day28"]);
                item.Day29 = Convert.ToInt32(reader["Day29"]);
                item.Day30 = Convert.ToInt32(reader["Day30"]);
                item.Day31 = Convert.ToInt32(reader["Day31"]);
                #endregion
                list.Add(item);
            }

            conn.Close();
            return list;
        }

        public List<LodgerMonthlyData> GetLodgerMonthly(DateTime from, DateTime to)
        {
            List<LodgerMonthlyData> list = new List<LodgerMonthlyData>();
            cmd = new SqlCommand("GetLodgerMonthly", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", from);
            cmd.Parameters.AddWithValue("toDate", to);

            conn.Open();
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                LodgerMonthlyData item = new LodgerMonthlyData();
                item.CheckIn = Convert.ToDateTime(reader["CheckIn"]);
                item.Qty = Convert.ToInt32(reader["Qty"]);
                list.Add(item);
            }

            conn.Close();
            return list;
        }

        public List<IncomeMonthlyData> GetIncomeMonthly(DateTime from, DateTime to)
        {
            List<IncomeMonthlyData> list = new List<IncomeMonthlyData>();
            cmd = new SqlCommand("GetIncomeMonthly", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", from);
            cmd.Parameters.AddWithValue("toDate", to);

            conn.Open();
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                IncomeMonthlyData item = new IncomeMonthlyData();
                item.CheckIn = Convert.ToDateTime(reader["CheckIn"]);
                item.CheckOut = Convert.ToDateTime(reader["CheckOut"]);
                item.Price = Convert.ToInt32(reader["Price"]);
                list.Add(item);
            }

            conn.Close();
            return list;
        }

        public List<RoomUsageMonthlyData> GetRoomUsageMonthly(DateTime from, DateTime to)
        {
            List<RoomUsageMonthlyData> list = new List<RoomUsageMonthlyData>();
            cmd = new SqlCommand("GetRoomUsageMonthly", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", from);
            cmd.Parameters.AddWithValue("toDate", to);

            conn.Open();
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                #region transform object
                RoomUsageMonthlyData item = new RoomUsageMonthlyData();
                item.RoomType = reader["RoomType"].ToString();
                item.Day1 = Convert.ToInt32(reader["Day1"]);
                item.Day2 = Convert.ToInt32(reader["Day2"]);
                item.Day3 = Convert.ToInt32(reader["Day3"]);
                item.Day4 = Convert.ToInt32(reader["Day4"]);
                item.Day5 = Convert.ToInt32(reader["Day5"]);
                item.Day6 = Convert.ToInt32(reader["Day6"]);
                item.Day7 = Convert.ToInt32(reader["Day7"]);
                item.Day8 = Convert.ToInt32(reader["Day8"]);
                item.Day9 = Convert.ToInt32(reader["Day9"]);
                item.Day10 = Convert.ToInt32(reader["Day10"]);
                item.Day11 = Convert.ToInt32(reader["Day11"]);
                item.Day12 = Convert.ToInt32(reader["Day12"]);
                item.Day13 = Convert.ToInt32(reader["Day13"]);
                item.Day14 = Convert.ToInt32(reader["Day14"]);
                item.Day15 = Convert.ToInt32(reader["Day15"]);
                item.Day16 = Convert.ToInt32(reader["Day16"]);
                item.Day17 = Convert.ToInt32(reader["Day17"]);
                item.Day18 = Convert.ToInt32(reader["Day18"]);
                item.Day19 = Convert.ToInt32(reader["Day19"]);
                item.Day20 = Convert.ToInt32(reader["Day20"]);
                item.Day21 = Convert.ToInt32(reader["Day21"]);
                item.Day22 = Convert.ToInt32(reader["Day22"]);
                item.Day23 = Convert.ToInt32(reader["Day23"]);
                item.Day24 = Convert.ToInt32(reader["Day24"]);
                item.Day25 = Convert.ToInt32(reader["Day25"]);
                item.Day26 = Convert.ToInt32(reader["Day26"]);
                item.Day27 = Convert.ToInt32(reader["Day27"]);
                item.Day28 = Convert.ToInt32(reader["Day28"]);
                item.Day29 = Convert.ToInt32(reader["Day29"]);
                item.Day30 = Convert.ToInt32(reader["Day30"]);
                item.Day31 = Convert.ToInt32(reader["Day31"]);
                #endregion
                list.Add(item);
            }

            conn.Close();
            return list;
        }

        public DateTime GetLatestDataDate()
        {
            cmd = new SqlCommand("SELECT Value FROM Configuration WHERE [Key] = 'HostelGraphDate'", conn);

            conn.Open();
            DateTime configDate = DateTime.ParseExact(cmd.ExecuteScalar().ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
            conn.Close();

            return configDate;
        }

        public void UpdateHostelUsage(DateTime from,DateTime to)
        {
            cmd = new SqlCommand("UpdateHostelUsage", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("fromDate", from);
            cmd.Parameters.AddWithValue("toDate", to);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

    }
}