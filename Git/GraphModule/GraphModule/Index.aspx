﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="GraphModule.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Goodluck n'Mon :)</title>
    <link href="~/Content/bootstrap.min.css" rel="stylesheet" />
    <link href="~/Content/site.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body>
    <div class="menu-box">
        <ul class="nav nav-pills nav-stacked">
            <li><a href="/Graph/Lodger.aspx">Lodger summary</a></li>
            <li><a href="/Graph/Nationality.aspx">Nationality summary</a></li>
            <li><a href="/Graph/Income.aspx">Income summary</a></li>
            <li><a href="/Graph/RoomUsage.aspx">Room usage summary</a></li>
        </ul>
    </div>
</body>
</html>
